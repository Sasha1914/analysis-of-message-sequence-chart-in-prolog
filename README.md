# Analysis of message sequence chart in  Prolog



##Overview

This project focuses on understanding and formalizing asynchronous communication models within the context of distributed systems. The implementation is done in Prolog, providing a practical exploration of various communication models using Message Sequence Charts (MSC).

##Introduction

Managing asynchronous communications is a significant challenge in evolving distributed systems. This project aims to deepen our understanding by formalizing and implementing asynchronous communication models using Prolog. The primary focus is on comparing different models through MSC analysis.

##Background

Distributed systems are sets of interconnected hardware or software components working together. Communication models, categorized as synchronous and asynchronous, play a vital role. Asynchronous models offer flexibility but require sophisticated mechanisms to handle delays and errors.

##Objectives

The main objectives of this project include formalizing communication models using MSC and implementing them in Prolog. The comparison of models helps analyze their behavior and performance in various distributed system scenarios.

##Usage

1-Clone the repository
2-Navigate to the project directory: cd asynchronous-communication-analyzer
3-Run Prolog with the main file: prolog main.pl
4-Follow the instructions to input MSC scenarios and analyze communication models.
