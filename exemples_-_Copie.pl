%exemple 1 :
message(a,b).
message(c,d).
juste_avant(a,c).
juste_avant(d,b).

%exemple 2 :
message(a,b).
message(c,d).
message(e,f).
juste_avant(a,c).
juste_avant(d,e).
juste_avant(f,b).

%exemple 3: 
message(a,b).
message(c,d).
juste_avant(d,a).
juste_avant(b,c).

%exemple 4 :
message(a,b).
message(c,d).
message(e,f).
message(g,h).
juste_avant(a,c).
juste_avant(h,b).
juste_avant(e,g).
juste_avant(d,f).

%exemple 5: 
message(a,b).
message(c,d).
message(e,f).
message(g,h).
juste_avant(a,d).
juste_avant(c,e).
juste_avant(f,g).
juste_avant(h,b).

%exemple 6:
message(a,b).
message(c,d).
juste_avant(a,d).
juste_avant(c,b).

%exemple 7 : 
message(a,b).
message(c,d).
message(e,f).
juste_avant(a,c).
juste_avant(d,f).
juste_avant(e,b).

%exemple 8 :
message(a,b).
message(c,d).
message(e,f).
juste_avant(b,f).
juste_avant(f,d).
juste_avant(c,e).

%exemple 9 :
message(a,b).
message(c,d).
message(e,f).
juste_avant(a,e).
juste_avant(d,f).
juste_avant(c,b).

%exemple 10 :
message(a,b).
message(c,d).
message(e,f).
juste_avant(a,f).
juste_avant(d,e).
juste_avant(e,b).

%exemple 11:
message_non_recu(a,p).
message(c,d).
juste_avant(a,c).
sur_machine(r,d).

%exemple 12:
message_non_recu(a,p).
message(c,d).
juste_avant(a,c).
sur_machine(p,d).


