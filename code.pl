%%% liste des matches:


%à définir


message(X,Y) :- fail. % fait "vide" pour que le pred message soit toujours defini

%%% liste des juste avant:


%à définir


%%% regles par defaut:
sur_machine(Q,Q).
message_non_reçu(M,M).

%%%regles:
avant_sur_meme_machine(X,Y) :- juste_avant(X,Y).
avant_sur_meme_machine(X,Y) :-juste_avant(X,Z),avant_sur_meme_machine(Z,Y).

meme_machine(X,Y) :-avant_sur_meme_machine(X,Y).
meme_machine(X,Y) :-avant_sur_meme_machine(Y,X).
meme_machine(X,Y) :-sur_machine(Q,X),sur_machine(Q,Y).


ordre_lamport(X,Y) :-avant_sur_meme_machine(X,Y).
ordre_lamport(X,Y) :-message(X,Y).
ordre_lamport(X,Y) :-avant_sur_meme_machine(X,Z), ordre_lamport(Z,Y).
ordre_lamport(X,Y) :-message(X,Z), ordre_lamport(Z,Y).

%diff(X,X) :- !, fail.
diff(X,Y) :-X \= Y .

p2p.
%p2p :- fail.
%mb.
mb :- fail.
%co.
co :- fail.
%rsc.
rsc :- fail.
%fifo_one_n.
fifo_one_n :- fail.
%fifo_n_n.
fifo_n_n :- fail.

avant(X,Y) :-message(X,Y).
avant(X,Y) :-avant_sur_meme_machine(X,Y).
avant(X,Y) :- p2p,
    meme_machine(X,Y),
    message(X,Z),
    message(Y,T),
    avant_sur_meme_machine(Z,T).
avant(X,Y) :- p2p,
    message(X,Z),
    message_non_reçu(Y,Q),
    sur_machine(Q,Z),
    meme_machine(X,Y).
avant(X,Y) :- mb,
    message(X,Z),
    message(Y,T),
    avant_sur_meme_machine(Z,T).
avant(X,Y) :-mb,
    message(X,Z),
    message_non_reçu(Y,Q),
    sur_machine(Q,Z).
avant(X,Y) :-co,
    message(Z,X),
    message(T,Y),
    meme_machine(X,Y),
    ordre_lamport(Z,T).
avant(X,Y) :-co,
    message(X,Z),
    message_non_reçu(Y,Q),
    sur_machine(Q,Z).
    ordre_lamport(Y,X)
avant(Y,Z) :-rsc,
    ordre_lamport(X,Y),
    message(X,Z),
    diff(Y,Z).
avant(X,Y) :- fifo_one_n,
    message(Z,X),
    message(T,Y),
    avant_sur_meme_machine(Z,T).
avant(X,Y) :-fifo_n_n,
    message(X,Z),
    message(Y,T),
    avant_sur_meme_machine(Z,T).
avant(Z,T) :-fifo_n_n,
    message(X,Z),
    message(Y,T),
    avant_sur_meme_machine(X,Y).
    

avant_plus(X,Y) :-avant(X,Y).
avant_plus(X,Y) :-avant(X,Z),avant_plus(Z,Y).

lasso(X,L,[Y,X|L]) :- avant(X,Y), in(Y,L), !.
lasso(X,L,Lasso) :- avant(X,Y), lasso(Y, [X|L],Lasso).

in(X, [X|_]).
in(X, [_|L]) :- in(X,L).
